# Ejemplos de privilegios y tipos básicos de DCL, así como los comandos GRANT y REVOKE para SQL Server, MariaDB y PostgreSQL

## SQL Server

### Crear usuario

```sql:
CREATE LOGIN usuarioEjemplo WITH PASSWORD = 'ContraseñaSegura123!';
CREATE USER usuarioEjemplo FOR LOGIN usuarioEjemplo;
```

### Conceder privilegios

```sql:
-- Conceder privilegios de selección en la tabla Departamento
GRANT SELECT ON Departamento TO usuarioEjemplo;

-- Conceder privilegios de inserción en la tabla Empleado
GRANT INSERT ON Empleado TO usuarioEjemplo;

-- Conceder privilegios de actualización en la tabla Proyecto
GRANT UPDATE ON Proyecto TO usuarioEjemplo;
```

### Revocar privilegios

```sql:
-- Revocar privilegios de selección en la tabla Departamento
REVOKE SELECT ON Departamento FROM usuarioEjemplo;

-- Revocar privilegios de inserción en la tabla Empleado
REVOKE INSERT ON Empleado FROM usuarioEjemplo;

-- Revocar privilegios de actualización en la tabla Proyecto
REVOKE UPDATE ON Proyecto FROM usuarioEjemplo;
```
## MariaDB

### Crear usuario

```sql:
CREATE USER 'usuarioEjemplo'@'localhost' IDENTIFIED BY 'ContraseñaSegura123!';
```

### Conceder privilegios

```sql:
-- Conceder privilegios de selección en la tabla Departamento
GRANT SELECT ON EmpresaDB.Departamento TO 'usuarioEjemplo'@'localhost';

-- Conceder privilegios de inserción en la tabla Empleado
GRANT INSERT ON EmpresaDB.Empleado TO 'usuarioEjemplo'@'localhost';

-- Conceder privilegios de actualización en la tabla Proyecto
GRANT UPDATE ON EmpresaDB.Proyecto TO 'usuarioEjemplo'@'localhost';
```

### Revocar privilegios

```sql:
-- Revocar privilegios de selección en la tabla Departamento
REVOKE SELECT ON EmpresaDB.Departamento FROM 'usuarioEjemplo'@'localhost';

-- Revocar privilegios de inserción en la tabla Empleado
REVOKE INSERT ON EmpresaDB.Empleado FROM 'usuarioEjemplo'@'localhost';

-- Revocar privilegios de actualización en la tabla Proyecto
REVOKE UPDATE ON EmpresaDB.Proyecto FROM 'usuarioEjemplo'@'localhost';
```

## PostgreSQL

### Crear usuario

```sql:
CREATE USER usuarioEjemplo WITH PASSWORD 'ContraseñaSegura123!';
```

### Conceder privilegios

```sql:
-- Conceder privilegios de selección en la tabla Departamento
GRANT SELECT ON TABLE Departamento TO usuarioEjemplo;

-- Conceder privilegios de inserción en la tabla Empleado
GRANT INSERT ON TABLE Empleado TO usuarioEjemplo;

-- Conceder privilegios de actualización en la tabla Proyecto
GRANT UPDATE ON TABLE Proyecto TO usuarioEjemplo;
```

### Revocar privilegios

```sql:
-- Revocar privilegios de selección en la tabla Departamento
REVOKE SELECT ON TABLE Departamento FROM usuarioEjemplo;

-- Revocar privilegios de inserción en la tabla Empleado
REVOKE INSERT ON TABLE Empleado FROM usuarioEjemplo;

-- Revocar privilegios de actualización en la tabla Proyecto
REVOKE UPDATE ON TABLE Proyecto FROM usuarioEjemplo;
```