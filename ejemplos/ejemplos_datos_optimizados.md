# Ejemplos sobre cómo usar tipos de datos optimizados y restricciones como DEFAULT, CHECK, UNIQUE e IDENTITY en la base de datos EmpleadoDB para SQL Server, MariaDB, SQLite y PostgreSQL.

## Tipos de datos optimizados:

* VARCHAR/NVARCHAR: Usado para cadenas de texto de longitud variable, optimiza el almacenamiento en comparación con tipos de longitud fija como CHAR.

* DECIMAL/NUMERIC/REAL: Usado para valores numéricos con decimales, precisando el uso de memoria en función del tamaño declarado.

* DATE: Almacena fechas de manera eficiente.

## Restricciones:

* DEFAULT: Provee un valor por defecto cuando no se especifica un valor para la columna.
* CHECK: Garantiza que los valores de la columna cumplen una condición específica.
* UNIQUE: Asegura que todos los valores en una columna son únicos.
* IDENTITY/AUTO_INCREMENT/SERIAL: Automáticamente incrementa el valor de la columna, útil para claves primarias.

## SQL Server

### Creación de la tabla con tipos de datos optimizados y restricciones

```sql:
CREATE DATABASE EmpleadoDB;
GO

USE EmpleadoDB;
GO

CREATE TABLE Departamento (
    IDDepartamento INT IDENTITY(1,1) PRIMARY KEY,
    NombreDepartamento NVARCHAR(50) NOT NULL UNIQUE,
    Ubicacion NVARCHAR(50) DEFAULT 'UNKNOWN'
);

CREATE TABLE Empleado (
    IDEmpleado INT IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado NVARCHAR(50) NOT NULL,
    Puesto NVARCHAR(50),
    IDJefe INT,
    FechaContratacion DATE DEFAULT GETDATE(),
    Salario DECIMAL(10,2) CHECK (Salario >= 0),
    Comision DECIMAL(10,2) CHECK (Comision >= 0),
    IDDepartamento INT,
    CONSTRAINT FK_Empleado_Departamento FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    CONSTRAINT FK_Empleado_Jefe FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)
);
```

## MariaDB

### Creación de la tabla con tipos de datos optimizados y restricciones

```sql:
CREATE DATABASE EmpleadoDB;
USE EmpleadoDB;

CREATE TABLE Departamento (
    IDDepartamento INT AUTO_INCREMENT PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL UNIQUE,
    Ubicacion VARCHAR(50) DEFAULT 'UNKNOWN'
);

CREATE TABLE Empleado (
    IDEmpleado INT AUTO_INCREMENT PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50),
    IDJefe INT,
    FechaContratacion DATE DEFAULT CURRENT_DATE,
    Salario DECIMAL(10,2) CHECK (Salario >= 0),
    Comision DECIMAL(10,2) CHECK (Comision >= 0),
    IDDepartamento INT,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)
);
```

## SQLite

### Creación de la tabla con tipos de datos optimizados y restricciones

```sql:
CREATE TABLE Departamento (
    IDDepartamento INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreDepartamento TEXT NOT NULL UNIQUE,
    Ubicacion TEXT DEFAULT 'UNKNOWN'
);

CREATE TABLE Empleado (
    IDEmpleado INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreEmpleado TEXT NOT NULL,
    Puesto TEXT,
    IDJefe INTEGER,
    FechaContratacion DATE DEFAULT (date('now')),
    Salario REAL CHECK (Salario >= 0),
    Comision REAL CHECK (Comision >= 0),
    IDDepartamento INTEGER,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)
);
```

## PostgreSQL

### Creación de la tabla con tipos de datos optimizados y restricciones

```sql:
CREATE DATABASE EmpleadoDB;

\c EmpleadoDB

CREATE TABLE Departamento (
    IDDepartamento SERIAL PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL UNIQUE,
    Ubicacion VARCHAR(50) DEFAULT 'UNKNOWN'
);

CREATE TABLE Empleado (
    IDEmpleado SERIAL PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50),
    IDJefe INT,
    FechaContratacion DATE DEFAULT CURRENT_DATE,
    Salario NUMERIC(10,2) CHECK (Salario >= 0),
    Comision NUMERIC(10,2) CHECK (Comision >= 0),
    IDDepartamento INT,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)
);
```