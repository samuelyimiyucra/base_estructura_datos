# SQL

Crearemos una base de datos EmpresaDB en SQL Server, MariaBD, SQlite y PostgreSQL

## Script de la base de datos

### SQL Server

```sql:
-- Crear base de datos
CREATE DATABASE EmpresaDB;
GO
USE EmpresaDB;
GO

-- Crear tabla Departamento
CREATE TABLE Departamento (
    IDDepartamento INT IDENTITY(1,1) PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL,
    Ubicacion VARCHAR(50) NOT NULL
);

-- Crear tabla Empleado
CREATE TABLE Empleado (
    IDEmpleado INT IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50) NOT NULL,
    IDJefe INT,
    FechaContratacion DATE NOT NULL,
    Salario DECIMAL(10, 2) NOT NULL,
    Comision DECIMAL(10, 2),
    IDDepartamento INT NOT NULL,
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla Proyecto
CREATE TABLE Proyecto (
    IDProyecto INT IDENTITY(1,1) PRIMARY KEY,
    NombreProyecto VARCHAR(50) NOT NULL,
    Ubicacion VARCHAR(50) NOT NULL,
    IDDepartamento INT NOT NULL,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla EmpleadoProyecto
CREATE TABLE EmpleadoProyecto (
    IDEmpleado INT NOT NULL,
    IDProyecto INT NOT NULL,
    HorasTrabajadas INT NOT NULL,
    PRIMARY KEY (IDEmpleado, IDProyecto),
    FOREIGN KEY (IDEmpleado) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDProyecto) REFERENCES Proyecto(IDProyecto)
);

-- Insertar registros en la tabla Departamento
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('RESEARCH', 'DALLAS');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('SALES', 'CHICAGO');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('OPERATIONS', 'BOSTON');

-- Insertar registros en la tabla Empleado sin jefes
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('KING', 'PRESIDENT', NULL, '1981-11-17', 5000, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JONES', 'MANAGER', 1, '1981-04-02', 2975, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('BLAKE', 'MANAGER', 1, '1981-05-01', 2850, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('CLARK', 'MANAGER', 1, '1981-06-09', 2450, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SCOTT', 'ANALYST', 2, '1987-04-19', 3000, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('FORD', 'ANALYST', 2, '1981-12-03', 3000, NULL, 2);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SMITH', 'CLERK', 3, '1980-12-17', 800, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ALLEN', 'SALESMAN', 4, '1981-02-20', 1600, 300, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('WARD', 'SALESMAN', 4, '1981-02-22', 1250, 500, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MARTIN', 'SALESMAN', 4, '1981-09-28', 1250, 1400, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('TURNER', 'SALESMAN', 4, '1981-09-08', 1500, 0, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ADAMS', 'CLERK', 5, '1987-05-23', 1100, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JAMES', 'CLERK', 4, '1981-12-03', 950, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MILLER', 'CLERK', 6, '1982-01-23', 1300, NULL, 1);

-- Insertar registros en la tabla Proyecto
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P1', 'BOSTON', 2);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P4', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P5', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P6', 'LOS ANGELES', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P8', 'NEW YORK', 3);

-- Insertar registros en la tabla EmpleadoProyecto
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 2, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 3, 12);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 2, 10);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 5, 8);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 1, 16);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 4, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 5, 5);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (11, 3, 6);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (12, 1, 4);
```

### MariaDB

```sql:
-- Crear base de datos
CREATE DATABASE IF NOT EXISTS EmpresaDB;
USE EmpresaDB;

-- Crear tabla Departamento
CREATE TABLE Departamento (
    IDDepartamento INT AUTO_INCREMENT PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL,
    Ubicacion VARCHAR(50) NOT NULL
);

-- Crear tabla Empleado
CREATE TABLE Empleado (
    IDEmpleado INT AUTO_INCREMENT PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50) NOT NULL,
    IDJefe INT,
    FechaContratacion DATE NOT NULL,
    Salario DECIMAL(10, 2) NOT NULL,
    Comision DECIMAL(10, 2),
    IDDepartamento INT NOT NULL,
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla Proyecto
CREATE TABLE Proyecto (
    IDProyecto INT AUTO_INCREMENT PRIMARY KEY,
    NombreProyecto VARCHAR(50) NOT NULL,
    Ubicacion VARCHAR(50) NOT NULL,
    IDDepartamento INT NOT NULL,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla EmpleadoProyecto
CREATE TABLE EmpleadoProyecto (
    IDEmpleado INT NOT NULL,
    IDProyecto INT NOT NULL,
    HorasTrabajadas INT NOT NULL,
    PRIMARY KEY (IDEmpleado, IDProyecto),
    FOREIGN KEY (IDEmpleado) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDProyecto) REFERENCES Proyecto(IDProyecto)
);

-- Insertar registros en la tabla Departamento
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('RESEARCH', 'DALLAS');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('SALES', 'CHICAGO');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('OPERATIONS', 'BOSTON');

-- Insertar registros en la tabla Empleado sin jefes
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('KING', 'PRESIDENT', NULL, '1981-11-17', 5000, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JONES', 'MANAGER', 1, '1981-04-02', 2975, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('BLAKE', 'MANAGER', 1, '1981-05-01', 2850, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('CLARK', 'MANAGER', 1, '1981-06-09', 2450, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SCOTT', 'ANALYST', 2, '1987-04-19', 3000, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('FORD', 'ANALYST', 2, '1981-12-03', 3000, NULL, 2);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SMITH', 'CLERK', 3, '1980-12-17', 800, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ALLEN', 'SALESMAN', 4, '1981-02-20', 1600, 300, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('WARD', 'SALESMAN', 4, '1981-02-22', 1250, 500, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MARTIN', 'SALESMAN', 4, '1981-09-28', 1250, 1400, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('TURNER', 'SALESMAN', 4, '1981-09-08', 1500, 0, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ADAMS', 'CLERK', 5, '1987-05-23', 1100, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JAMES', 'CLERK', 4, '1981-12-03', 950, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MILLER', 'CLERK', 6, '1982-01-23', 1300, NULL, 1);

-- Insertar registros en la tabla Proyecto
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P1', 'BOSTON', 2);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P4', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P5', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P6', 'LOS ANGELES', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P8', 'NEW YORK', 3);

-- Insertar registros en la tabla EmpleadoProyecto
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 2, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 3, 12);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 2, 10);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 5, 8);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 1, 16);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 4, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 5, 5);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (11, 3, 6);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (12, 1, 4);
```

### SQLite

```sql:
-- Crear tabla Departamento
CREATE TABLE Departamento (
    IDDepartamento INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreDepartamento TEXT NOT NULL,
    Ubicacion TEXT NOT NULL
);

-- Crear tabla Empleado
CREATE TABLE Empleado (
    IDEmpleado INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreEmpleado TEXT NOT NULL,
    Puesto TEXT NOT NULL,
    IDJefe INTEGER,
    FechaContratacion TEXT NOT NULL,
    Salario REAL NOT NULL,
    Comision REAL,
    IDDepartamento INTEGER NOT NULL,
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla Proyecto
CREATE TABLE Proyecto (
    IDProyecto INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreProyecto TEXT NOT NULL,
    Ubicacion TEXT NOT NULL,
    IDDepartamento INTEGER NOT NULL,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla EmpleadoProyecto
CREATE TABLE EmpleadoProyecto (
    IDEmpleado INTEGER NOT NULL,
    IDProyecto INTEGER NOT NULL,
    HorasTrabajadas INTEGER NOT NULL,
    PRIMARY KEY (IDEmpleado, IDProyecto),
    FOREIGN KEY (IDEmpleado) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDProyecto) REFERENCES Proyecto(IDProyecto)
);

-- Insertar registros en la tabla Departamento
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('RESEARCH', 'DALLAS');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('SALES', 'CHICAGO');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('OPERATIONS', 'BOSTON');

-- Insertar registros en la tabla Empleado sin jefes
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('KING', 'PRESIDENT', NULL, '1981-11-17', 5000, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JONES', 'MANAGER', 1, '1981-04-02', 2975, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('BLAKE', 'MANAGER', 1, '1981-05-01', 2850, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('CLARK', 'MANAGER', 1, '1981-06-09', 2450, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SCOTT', 'ANALYST', 2, '1987-04-19', 3000, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('FORD', 'ANALYST', 2, '1981-12-03', 3000, NULL, 2);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SMITH', 'CLERK', 3, '1980-12-17', 800, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ALLEN', 'SALESMAN', 4, '1981-02-20', 1600, 300, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('WARD', 'SALESMAN', 4, '1981-02-22', 1250, 500, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MARTIN', 'SALESMAN', 4, '1981-09-28', 1250, 1400, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('TURNER', 'SALESMAN', 4, '1981-09-08', 1500, 0, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ADAMS', 'CLERK', 5, '1987-05-23', 1100, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JAMES', 'CLERK', 4, '1981-12-03', 950, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MILLER', 'CLERK', 6, '1982-01-23', 1300, NULL, 1);

-- Insertar registros en la tabla Proyecto
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P1', 'BOSTON', 2);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P4', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P5', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P6', 'LOS ANGELES', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P8', 'NEW YORK', 3);

-- Insertar registros en la tabla EmpleadoProyecto
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 2, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 3, 12);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 2, 10);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 5, 8);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 1, 16);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 4, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 5, 5);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (11, 3, 6);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (12, 1, 4);
```

### PostgreSQL

```sql:
-- Crear la base de datos
CREATE DATABASE EmpleadoDB;

-- Conectar a la base de datos
\c EmpleadoDB;

-- Crear tabla Departamento
CREATE TABLE Departamento (
    IDDepartamento SERIAL PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL UNIQUE,
    Ubicacion VARCHAR(50)
);

-- Crear tabla Empleado
CREATE TABLE Empleado (
    IDEmpleado SERIAL PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50),
    IDJefe INT REFERENCES Empleado(IDEmpleado),
    FechaContratacion DATE,
    Salario DECIMAL(10,2),
    Comision DECIMAL(10,2),
    IDDepartamento INT REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla Proyecto
CREATE TABLE Proyecto (
    IDProyecto SERIAL PRIMARY KEY,
    NombreProyecto VARCHAR(50) NOT NULL,
    Ubicacion VARCHAR(50),
    IDDepartamento INT REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla EmpleadoProyecto
CREATE TABLE EmpleadoProyecto (
    IDEmpleado INT REFERENCES Empleado(IDEmpleado),
    IDProyecto INT REFERENCES Proyecto(IDProyecto),
    HorasTrabajadas INT,
    PRIMARY KEY (IDEmpleado, IDProyecto)
);

-- Insertar registros en la tabla Departamento
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('RESEARCH', 'DALLAS');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('SALES', 'CHICAGO');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('OPERATIONS', 'BOSTON');

-- Insertar registros en la tabla Empleado sin jefes
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('KING', 'PRESIDENT', NULL, '1981-11-17', 5000, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('JONES', 'MANAGER', 1, '1981-04-02', 2975, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('BLAKE', 'MANAGER', 1, '1981-05-01', 2850, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('CLARK', 'MANAGER', 1, '1981-06-09', 2450, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('SCOTT', 'ANALYST', 2, '1987-04-19', 3000, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('FORD', 'ANALYST', 2, '1981-12-03', 3000, NULL, 2);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('SMITH', 'CLERK', 5, '1980-12-17', 800, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('ALLEN', 'SALESMAN', 3, '1981-02-20', 1600, 300, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('WARD', 'SALESMAN', 3, '1981-02-22', 1250, 500, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('MARTIN', 'SALESMAN', 3, '1981-09-28', 1250, 1400, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('TURNER', 'SALESMAN', 3, '1981-09-08', 1500, 0, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('ADAMS', 'CLERK', 4, '1987-05-23', 1100, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('JAMES', 'CLERK', 3, '1981-12-03', 950, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) 
VALUES ('MILLER', 'CLERK', 4, '1982-01-23', 1300, NULL, 1);

-- Insertar registros en la tabla Proyecto
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P1', 'BOSTON', 2);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P4', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P5', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P6', 'LOS ANGELES', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P8', 'NEW YORK', 3);

-- Insertar registros en la tabla EmpleadoProyecto
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 2, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 3, 12);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 2, 10);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 5, 8);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 1, 16);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 4, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 5, 5);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (11, 3, 6);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (14, 1, 4);
```

## Ejercicios resueltos

**1. Consulta de Departamentos:**

¿Cuáles son los identificadores y nombres de todos los departamentos existentes en la empresa?

Solucion:

```sql:
SELECT IDDepartamento, NombreDepartamento 
FROM Departamento;
```

**2. Consulta de Jefes:**

¿Cuáles son los códigos de los jefes que actualmente tienen empleados a su cargo?

Solucion:

```sql:
SELECT DISTINCT IDJefe AS "Codigo del jefe"
FROM Empleado
WHERE IDJefe IS NOT NULL;
```

**3. Consulta de Ubicación:**

¿Cuál es la ubicación del departamento identificado con el número 3?

Solucion:

```sql:
SELECT Ubicacion
FROM Departamento
WHERE IDDepartamento = 3;
```

**4. Consulta de Empleados sin Jefe:**

¿Cuáles son los nombres de los empleados que no están asignados a ningún jefe?

Solucion:

```sql:
SELECT NombreEmpleado
FROM Empleado
WHERE IDJefe IS NULL;
```

**5. Consulta de Salarios Superiores:**

¿Cuáles son los detalles de los empleados que tienen un jefe y cuyo salario, incluyendo o no la comisión, supera los 2500?

Solucion:

```sql:
SELECT *
FROM Empleado
WHERE IDJefe IS NOT NULL AND 
      (Salario + COALESCE(Comision, 0) > 2500 OR Salario > 2500);
```

**6. Consulta de Nombres de Empleados:**

¿Cuáles son los detalles de los empleados cuyos nombres comienzan con la letra 'S'?

Solucion:

```sql:
SELECT *
FROM Empleado
WHERE NombreEmpleado LIKE 'S%';
```

**7. Consulta de Rango Salarial:**

¿Cuáles son los detalles de los empleados cuyo salario, con o sin comisión, se encuentra entre 1500 y 2500, o aquellos que no tienen comisión y cuyo salario está dentro de ese rango?

Solucion:

```sql:
SELECT *
FROM Empleado
WHERE (Comision IS NULL AND Salario BETWEEN 1500 AND 2500) OR 
      (Salario + COALESCE(Comision, 0) BETWEEN 1500 AND 2500);
```

**8. Consulta de Puestos y Salarios:**

¿Cuáles son los detalles de los empleados que ocupan los puestos de 'CLERK', 'SALESMAN' o 'ANALYST', y cuyo salario, con o sin comisión, es superior a 1250?

Solucion:

```sql:
SELECT *
FROM Empleado
WHERE Puesto IN ('CLERK', 'SALESMAN', 'ANALYST') AND 
      (Salario > 1250 OR Salario + COALESCE(Comision, 0) > 1250);
```

**9. Agrupación por Departamento:**

¿Cuántos empleados hay en cada departamento, cuántos reciben comisión, cuántos no la reciben, y cuál es el salario promedio (incluyendo comisiones) por departamento?

Solucion:

```sql:
SELECT IDDepartamento AS "Numero de departamento",
       COUNT(NombreEmpleado) AS "Empleados por departamento",
       SUM(CASE WHEN Comision IS NOT NULL THEN 1 ELSE 0 END) AS "Empleados con comision",
       SUM(CASE WHEN Comision IS NULL THEN 1 ELSE 0 END) AS "Empleados sin comision",
       AVG(Salario + COALESCE(Comision, 0)) AS "Media de salarios + comision"
FROM Empleado
GROUP BY IDDepartamento;
```

**10. Departamentos con Comisión:**

¿Cuáles son los departamentos que tienen al menos un empleado que recibe comisión?

Solucion:

```sql:
SELECT DISTINCT IDDepartamento
FROM Empleado
WHERE Comision IS NOT NULL;
```

**11. Promedio de Comisión por Departamento:**

¿Cuál es la comisión promedio por departamento?

Solucion:

```sql:
SELECT IDDepartamento, COALESCE(AVG(Comision), 0) AS "Promedio de Comision"
FROM Empleado
GROUP BY IDDepartamento;
```

**12. Diversidad de Puestos de Trabajo:**

¿Cuántos tipos distintos de puestos de trabajo existen en cada departamento?

Solucion:

```sql:
SELECT IDDepartamento, COUNT(DISTINCT Puesto) AS "Puestos de trabajo distintos"
FROM Empleado
GROUP BY IDDepartamento;
```

## Ejercicios propuestos

**1. Consulta de Proyectos:**

¿Cuáles son los identificadores y nombres de todos los proyectos existentes en la empresa?

**2. Consulta de Proyectos por Ubicación:**

¿Cuáles son los proyectos que se desarrollan en 'CHICAGO'?

**3. Consulta de Proyectos por Departamento:**

¿Cuáles son los proyectos que pertenecen al departamento con identificador 2?

**4. Consulta de Proyectos y Departamentos:**

¿Cuáles son los nombres y ubicaciones de los proyectos junto con los nombres de sus departamentos asociados?

**5. Consulta de Empleados por Proyecto:**

¿Qué empleados están asignados al proyecto identificado con el número 4, y cuáles son sus nombres?

**6. Consulta de Proyectos por Empleado:**

 ¿En qué proyectos está participando el empleado con el identificador 4, y cuáles son los nombres de esos proyectos?

**7. Consulta de Horas Trabajadas por Proyecto:**

¿Cuántas horas han trabajado en total los empleados en el proyecto con identificador 2?

**8. Consulta de Empleados con Horas Trabajadas:**

¿Cuáles son los empleados que han trabajado más de 10 horas en el proyecto con identificador 2?

**9. Consulta de Total de Horas por Empleado:**

¿Cuál es el total de horas trabajadas por cada empleado en todos los proyectos?

**10. Consulta de Empleados con Múltiples Proyectos:**

¿Cuáles son los empleados que trabajan en más de un proyecto?

**11. Consulta de Empleados y Horas Totales:**

¿Cuáles son los empleados que han trabajado más de 30 horas en total en todos los proyectos?

**12. Consulta de Proyectos y Horas Promedio:**

¿Cuál es el promedio de horas trabajadas por proyecto?

## Consultas avanzadas

### Pregunta 1: Empleados en Proyectos Específicos y con Salario Alto

¿Cuáles son los empleados que trabajan en proyectos ubicados en 'CHICAGO' y que tienen un salario (con o sin comisión) superior a 2000?

### Pregunta 2: Empleados con Jefe y en Proyectos Múltiples

¿Cuáles son los empleados que tienen un jefe, están asignados a más de un proyecto, y han trabajado más de 15 horas en total en todos los proyectos combinados?

### Pregunta 3: Empleados sin Comisión en Departamentos Específicos

¿Cuáles son los empleados que no reciben comisión y trabajan en departamentos ubicados en 'DALLAS' o 'NEW YORK'?


