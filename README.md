# Curso: Base y estructura de datos

## Intructor: Ing. Franklin Condori

[* Sesión 01 ](/sesion_01/sesion_01.md)

[* Practica calificada 01 ](/practicas_calificadas/practica_01.md)

[* Ejemplos privilegios](/ejemplos/ejemplos_privilegios.md)


[* Ejemplos DCL](/ejemplos/ejemplos_dcl.md)

[* Ejemplos datos optimizados](/ejemplos/ejemplos_datos_optimizados.md)

[* Ejemplos varios](/ejemplos/ejemplos_varios.md)
